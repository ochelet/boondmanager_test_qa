# Test technique BoondManager

Ce test se déroule en 2 parties.

## Partie rédactionnelle

__durée indicative: 30 minutes__

Dans le répertoire cdc, vous trouverez un cahier des charges sous forme de document Word.

Il s'agit d'un cahier des charges fictif, composé de 9 US. Dans chacune de ces US se trouve une section Cas de test à compléter.

Merci de rédiger des cas de test pour les US suivantes :

* US1
* US3

Complétez directement le document, posez vos éventuelles questions sur une nouvelle page du document, et faites un commit

## Partie technique

__durée: 2h30__

La deuxième partie consiste en la mise en place d'un test automatisé sur une application web très simple.

L'application web est entièrement conteneurisée. Elle devrait tourner toute seule en suivant ce guide.

### Pré-requis

Pour cette deuxième partie, vous devrez installer Docker sur votre poste de travail. La méthode d'installation est disponible à cette adresse :

https://docs.docker.com/engine/install/

Ainsi que docker-compose (déjà installé si vous avez installé Docker Desktop) :

https://docs.docker.com/compose/install/

Une fois cela fait, vous devriez avoir les numéros de versions installés lorsque vous lancez les commandes suivantes :

```
➜  boondmanager_test_qa > docker --version
Docker version 20.10.5, build 55c4c88
➜  boondmanager_test_qa > docker-compose --version
docker-compose version 1.28.5, build c4eb3a1f
```

Les numéros de versions peuvent bien entendu différer.

### Mise en place de l'environnement

En ligne de commande, allez à la racine du dépôt, et tapez les commandes suivantes :

```
➜  boondmanager_test_qa > docker-compose build
➜  boondmanager_test_qa > docker-compose up
```

La commande build ira chercher les dépendances nécessaires, et sera un peu longue la première fois.

La commande up va lancer l'application, qui sera alors disponible via l'url suivante : http://localhost:8000/

Si vous avez besoin de réinitialisez les données, vous pouvez, dans un autre terminal, lancer les commandes suivantes :

```
➜  boondmanager_test_qa > docker-compose run web python manage.py flush
➜  boondmanager_test_qa > docker-compose run web python manage.py loaddata fixtures/initial_data.json
```

Ou bien, stoppez le process docker-compose up avec ctrl+c et faites :

```
➜  boondmanager_test_qa > docker-compose down -v
➜  boondmanager_test_qa > docker-compose up
```

### L'exercice

Dans le répertoire test, utilisez vos outils de test automatique favoris pour automatiser le scénario suivant :

```
Scénario: Les gens disent majoritairement Pain au chocolat
    Etant donné que je consulte la page "http://localhost:8000/"
    Quand je clique sur le lien "Quel est le bon nom ?"
    Alors je suis sur la page "http://localhost:8000/4/
    Et le titre de la page est "Quel est le bon nom ?"
    Et les réponses suivantes sont proposées:
    | réponse          |
    | Chocolatine      |
    | Pain au chocolat |
    Quand je sélectionne la réponse "Pain au chocolat"
    Et que je clique sur le bouton "Vote"
    Alors le texte "Pain au chocolat -- 33 votes" est affiché
```

Veuillez noter vos choix technique dans un fichier readme placé à la racine du répertoire tests, avec les instructions pour lancer le test.

### Bonus

Si vous en avez le temps, n'hésitez pas à dockeriser votre stack de test et intégrez là dans le docker-compose

### Comment envoyer le résultat

Idéalement, créez un fork du présent repository et ajoutez votre code.

Sinon, envoyez une archive zip par email à <thomas.durey@wishgroupe.com>

## En cas de problèmes

- Ne passez pas plus de 30 minutes sur la partie rédactionnelle
- Au moindre problème, n'hésitez pas à m'appeler en cas de soucis technique

Si vous n'arrivez pas à mettre en place l'environnement de test, automatisez le scénario suivant:

```
Scénario: Consultation du site BoondManager
    Etant donné que je consulte la page "https://www.boondmanager.com/"
    Quand je clique sur le lien "Société"
    Alors je suis sur la page "https://www.boondmanager.com/la-societe-boondmanager/
    Et le titre de la page est "Société"
    Et le texte "nous savons qu’un ERP généraliste ne répondrait pas à vos processus métiers" est affiché
    Quand je clique sur le lien "Blog"
    Alors je suis sur la page "https://www.boondmanager.com/notre-blog/"
    Quand je rempli le champ de recherche avec le texte "test auto"
    Et que je clique sur le bouton "Rechercher"
    Alors je suis sur la page "https://www.boondmanager.com/?s=test+auto"
    Et 8 résultats sont affichés
```
